﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerRotacion : MonoBehaviour
{
    public CharacterController _controller;
    public float _speed = 10;
    public float _rotationSpeed = 180;
    public AudioSource walk;
    public Vector3 ultimaPosicion;
    public bool alturaFix;
    public float altura;

    private Vector3 rotation;

    public void Update()
    {

        moverPlayer();
        FixFloor();
        seMueve(gameObject);
        
    }

    public void moverPlayer()
    {
        this.rotation = new Vector3(0, Input.GetAxisRaw("Horizontal") * _rotationSpeed * Time.deltaTime, 0);

        Vector3 move = new Vector3(0, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime);
        move = this.transform.TransformDirection(move);
        _controller.Move(move * _speed);
        this.transform.Rotate(this.rotation);
    }

    public void seMueve(GameObject player)
    {

        if (ultimaPosicion != player.gameObject.transform.position)
        {
            walk.enabled = true;
        }
        else { walk.enabled = false; }
        ultimaPosicion = player.gameObject.transform.position;
    }

    public void FixFloor()
    {
        if (alturaFix)
        {

            gameObject.transform.position = new Vector3(gameObject.transform.position.x, altura, gameObject.transform.position.z);
        }
    }
    
}
