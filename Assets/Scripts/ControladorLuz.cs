﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorLuz : MonoBehaviour
{
    public Light light;
    // Start is called before the first frame update
    void Start()
    {
        light = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            light.spotAngle = light.spotAngle + 10 * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            light.spotAngle = light.spotAngle - 10 * Time.deltaTime;
        }
    }
}
