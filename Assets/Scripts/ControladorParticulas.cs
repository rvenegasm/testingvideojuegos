﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorParticulas : MonoBehaviour
{

    public ParticleSystem ps;
    public bool emitiendo;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ps.isEmitting)
            {
               
                ps.Stop();
                Debug.Log("deteniendo las particulas");
                emitiendo = false;
            }
            else 
            {
                
                ps.Play();
                Debug.Log("emitiendo las particulas");
                emitiendo = true;
            }
        }
    }
}
