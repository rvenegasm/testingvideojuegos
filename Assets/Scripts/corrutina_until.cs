﻿using UnityEngine;
using System.Collections;


/*Suspende la ejecución de la corrutina hasta que el delegado proporcionado se evalúa como verdadero.

WaitUntil solo se puede usar con una declaración de rendimiento en corrutinas.*/
public class corrutina_until : MonoBehaviour
{
    public float segundos;


    void Start()
    {
        StartCoroutine(Loop());
    }

    IEnumerator Loop()//usaremos el white until, hasta que se cumpla la condición
    {
        Debug.Log("esprando al rescate...");
        yield return new WaitUntil(() => segundos >= 10);
        Debug.Log("¡La princesa fue rescatada!");
    }

    void Update()
    {
        if (segundos <= 10)
        {
            Debug.Log("segundos: " + segundos.ToString("f0"));
            segundos = segundos + Time.deltaTime;
        }
    }
}