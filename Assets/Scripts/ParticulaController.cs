﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulaController : MonoBehaviour
{
    public ParticleSystem ps;
    public bool emitiendo;


    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ps.isEmitting)
            {
                ps.Stop();
                Debug.Log("se deteniendo las particulas");
                emitiendo = false;
            }
            else
            {
                ps.Play();
                Debug.Log("se activando las particulas");
                emitiendo = true;
            }


       }
    }
}
