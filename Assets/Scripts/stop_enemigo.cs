﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class stop_enemigo : MonoBehaviour
{
   
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.tag == "Enemigo")
        {
         
            hit.collider.GetComponent<SphereCollider>().enabled = false;
            hit.collider.GetComponent<persecucion>().colision = true;
            hit.collider.GetComponent<persecucion>().enabled = true;
            
            Debug.Log(hit.collider.name + " detenido");

          

        }
    }
}
