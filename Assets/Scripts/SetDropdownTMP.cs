﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetDropdownTMP : MonoBehaviour
{

    public TMP_Dropdown dropdown;

    private void Start()
    {
        dropdown = GetComponent<TMP_Dropdown>();
    }

    public void SetDropdown()
    {
        Debug.Log("Dropdown -> " + dropdown.options[dropdown.value].text);
    }


}
