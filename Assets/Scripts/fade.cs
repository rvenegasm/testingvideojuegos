﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fade : MonoBehaviour
{

    public bool onoff;
    public bool fading = false;
    public float alpha = 1;


    void Fade()
    {

        Debug.Log("hola, estoy haciendo un fade");
        while (alpha > 0)
        {
            Color c = GetComponent<Renderer>().material.color;
            c.a = alpha;

            GetComponent<Renderer>().material.color = c;
            alpha = alpha - 0.01f;

        }
    }

    void UnFade()
    {

        Debug.Log("hola, estoy haciendo un unfade");
        while (alpha < 1)
        {
            Color c = GetComponent<Renderer>().material.color;
            c.a = alpha;

            GetComponent<Renderer>().material.color = c;
            alpha = alpha + 0.01f;

        }
    }

    //IEnumerator FadeCorrutina()
    //{
    //    Debug.Log("hola, estoy haciendo un fade");
    //    while (alpha > 0)
    //    {
    //        Color c = GetComponent<Renderer>().material.color;
    //        c.a = alpha;

    //        GetComponent<Renderer>().material.color = c;
    //        alpha = alpha - 0.01f;
    //        yield return new WaitForSeconds(0.03f);
    //    }
    //}

    //IEnumerator UnFadeCorrutina()
    //{
    //    Debug.Log("hola, estoy haciendo un unfade");
    //    while (alpha < 1)
    //    {
    //        Color c = GetComponent<Renderer>().material.color;
    //        c.a = alpha;

    //        GetComponent<Renderer>().material.color = c;
    //        alpha = alpha + 0.01f;
    //        yield return new WaitForSeconds(0.03f);
    //    }
    //}

    private void Start()
    {
       // StartCoroutine(FadeCorrutina());
    }
    void Update()
    {
        //// Toggle Pausa
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    if (!onoff)
        //    {
        //        onoff = true;

        //        Debug.Log("fadeandoooo");
        //        StartCoroutine(FadeCorrutina());
        //    }
        //    else
        //    {
        //        onoff = false;

        //        Debug.Log("desfadeandoooo");
        //        StartCoroutine(UnFadeCorrutina());
        //    }
        //}

        if (fading)
        {


            Fade();
            fading = true;


        }
        else
        {
            UnFade();
            fading = false;
        }

    }
}
