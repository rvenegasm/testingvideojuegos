﻿using UnityEngine;
using System.Collections;

/*Suspende la ejecución de la corrutina hasta que el delegado proporcionado se evalúa como falso.

WaitWhile solo se puede usar con una declaración de rendimiento en corrutinas.*/
public class corrutina_while : MonoBehaviour
{
    public float segundos;
    

    void Start()
    {
        StartCoroutine(Loop());
    }

    IEnumerator Loop()//usaremos el while, mientras cumpla la condición
    {
        Debug.Log("Esperando que la princesa sea rescatada...");
        yield return new WaitWhile(() => segundos < 10);
        Debug.Log("¡Finalmente me han rescatado!");
    }

    void Update()
    {
        if (segundos <= 10)
        {
            Debug.Log("segundos: " + segundos.ToString("f0"));
            segundos = segundos + Time.deltaTime;
        }
    }
}