﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DropdownSet : MonoBehaviour
{

    public Dropdown dropdown;

    private void Start()
    {
        dropdown = GetComponent<Dropdown>();
    }

    public void SetDropdown()
    {
        Debug.Log("Dropdown -> " + dropdown.options[dropdown.value].text);
    }
}
