﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class corrutina : MonoBehaviour
{ 
//se define siempre con un yield return, es necesario decir cuando termina la corrutina


    //se define bajo un IEnumerator
    //se lanza con un StartCoroutine
    //se detiene cuando captura el return o cuando fuerza la detencion con un stop
    //las condiciones mas usadas son WaitForSeconds, WaitWhile, WaitUntil
    //incluso se puede lanzar la misma corrutina de forma recursiva

    IEnumerator CorrutinaTest()
    {
        Debug.Log("hola soy un corrutina");

        // yield return 0;// puede ser null o 0 es decir, no nos interesa que devuelva algo
        yield return new WaitForSeconds(3);
        Debug.Log("pasaron 3 seg");
        //OJO tambien podemos detener corrutinas
    }

    //OJO! el start tambien se puede convertir en corrutina!!!!
    private void Start()
    {
        StartCoroutine(CorrutinaTest());
        
    }
}
