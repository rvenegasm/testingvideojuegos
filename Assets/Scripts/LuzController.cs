﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzController : MonoBehaviour
{
    public Light luz;



    private void Start()
    {
        luz = GetComponent<Light>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            luz.spotAngle = luz.spotAngle + 10 * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            luz.spotAngle = luz.spotAngle - 10 * Time.deltaTime;
        }
    }



}
