﻿using UnityEngine;
using System.Collections.Generic;

/*
Invoca el método methodName en segundos, luego repetidamente cada repeatRate segundos.
// Comenzando en 2 segundos.
// se lanzará un proyectil cada 0.3 segundos 
*/

public class invoke : MonoBehaviour
{
    public Rigidbody projectile;

    void Start()
    {
        Invoke("lanzaProyectil",2.5f);
        InvokeRepeating("LanzaProyectil", 2.0f, 0.3f);
        //OJO tambien podemos cancelar corrutinas
        
    }

    void LanzaProyectil()
    {
        Debug.Log("Pium!!! ahi voy!");
    }
}