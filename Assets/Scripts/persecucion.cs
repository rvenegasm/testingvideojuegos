﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class persecucion : MonoBehaviour
{
    public Transform objetivo;
    private NavMeshAgent agente;
    public bool colision = false;
    public int segundos = 0;
    // Start is called before the first frame update
    void Start()
    {
        agente = GetComponent<NavMeshAgent>();

        StartCoroutine(seguir());

    }

   IEnumerator seguir()
    {

        while (colision == false)
        {
            Debug.Log(gameObject.name +  " persiguiendo al player");
            agente.destination = objetivo.position;
            segundos++;
            yield return new WaitForSeconds(1);//espero 12 segundo y vuelvo a hacer el while consultando si se cumplio el objetivo de colisionar con el player
            
        }
        if (colision == true)
        {
            Stop();
        }
        yield return 0;
    }

    public void Stop()
    {
        Debug.Log(gameObject.name + " persiguio al objetivo por "+ segundos +" segundos");
        
    }
}
