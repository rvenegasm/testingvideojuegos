﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento_player : MonoBehaviour
{
    public float movimientoHorizontal;
    public float movimientoVertical;
    public CharacterController player;
    public float velocidadPlayer = 2;

    private void Start()
    {
        player = GetComponent<CharacterController>();
    }

    private void Update()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
        player.Move(new Vector3(movimientoHorizontal, 0, movimientoVertical) * velocidadPlayer *Time.deltaTime);
    }
}
